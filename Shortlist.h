#ifndef SHORTLIST_H
#define SHORTLIST_H

// * DNT
#include <stdbool.h>
#include <stdlib.h>

#ifndef ITEM_DEF
#define ITEM_DEF
typedef void *item;
#endif // ! ITEM_DEF

typedef struct ShortList *shortLink;
struct ShortList {
    item Item;
    shortLink Next;
};
// * EDNT

shortLink validItemsListWithCount(shortLink HEAD, bool (*valid)(item i, item args), item args, unsigned int *numberOfItems);
shortLink validItemsList(shortLink HEAD, bool (*valid)(item i, item args), item args);
item searchByPointer(shortLink list, item PointerValue);
item searchByID(shortLink HEAD, item ID, bool (*matchID)(item i, item ID));
void putItem(shortLink list, item i);
void putLink(shortLink list, shortLink l);
void pushItem(shortLink list, item i);
void pushLink(shortLink HEAD, shortLink l);
item pullLink(shortLink HEAD);
item pullItem(shortLink HEAD);
item popLink(shortLink list);
item popItem(shortLink list);
shortLink getTail(shortLink l);
shortLink getPreTail(shortLink l);
item getListItemByIndex(shortLink HEAD, unsigned int i);
void freeList(shortLink HEAD);
unsigned int countValidItemsList(shortLink HEAD, bool (*valid)(item i, item args), item args);
bool getNext(shortLink *l);
unsigned int countItemsList(shortLink HEAD);
shortLink newLink(item i);
void addLinkInOrder(shortLink HEAD, shortLink l, bool (*minor)(item a, item l, item args), item args);
void addItemInOrder(shortLink HEAD, item i, bool (*minor)(item a, item l, item args), item args);
bool matchAddress(item i, item ID);

#endif // ! SHORTLIST_H
